#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <string.h>
#include <iostream>

using namespace std;

// définition des variables de portée globale

// tableau de 10 chaines de caractères contenant les mots à "faire trouver"
// TODO
QString mot[10] = {"VADOR",
                   "ANAKIN",
                   "SNOKE",
                   "REN",
                   "REY",
                   "LUKE",
                   "LEIA",
                   "SOLO",
                   "LANDO",
                   "YODA"
                  };

// le mot en cours de construction (ce que voit le joueur, au départ une suite de '*')
QString chaine;

// erreur : compte le nombre de caractères demandés mais non présent dans le mot
// rappel : il y a 12 échecs possibles avant d'être pendu
int erreur;

// bon : sera incrémentée à chaque fois qu'une bonne lettre aura été placée -
// permet donc de savoir si on a trouvé le mot et du coup si on a gagné
// en effet ==> 'bon' doit avoir la même valeur que la longueur du mot à trouver (longueur = nombre de caractères composant la chaîne 'mot')
int bon;

// i : utilisé comme indice pour parcourir le tableau mot
// (à chaque fois que le joueur veut re-jouer, il faudra passer à l'indice suivant (mot suivant)--> i++
int i = 0;


// méthode de construction de la fenêtre (formulaire)  // NE PAS MODIFIER
MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  initialisation();   //*****  appel à la méthode initialisation    *****//
}

// méthode de "destruction" de la fenêtre (formulaire)  // NE PAS MODIFIER
MainWindow::~MainWindow()
{
  delete ui;
}

//*******  METHODE initialisation  *******//
void MainWindow::initialisation()
{
  /* "prépare" le formulaire de jeu :
   * - mettre dans le label "mot_a_deviner" autant de caractères '*' que de lettres dans le mot à faire deviner - TODO
  */
    chaine = "";
    for(int c = 0 ; c < mot[i].length();c++)
    {
        chaine.append('*');
    }
    ui->mot_a_deviner->setText(chaine);

  /*
   * - mise à 0 des variables servant à compter les erreurs et les bonnes lettres trouvées  - TODO
  */
    erreur = 0;
    bon = 0;
  /*
   * - rendre invisibles les boutons Re-jouer et Quitter   - TODO
  */
    ui->rejouer->setVisible(false);
    ui->quitter->setVisible(false);

  /*
   * - activer le groupbox des lettres - TODO  ainsi que tous les boutons lettres  - OK
  */
    ui->lettres->setVisible(true);
    ui->lettres->setEnabled(true);

  /*
   * - mettre le label de message (gagné ou perdu) à vide   - TODO
  */
    ui->message->setVisible(true);
    ui->message->setText(" ");
  /*
   * - placer la première image (0.jpg) dans le label correspondant   - TODO
   */
    ui->image->setPixmap(QPixmap("images/0.jpg"));


  // activer tous les boutons lettres contenus dans le groupbox lettres  // NE PAS MODIFIER
  for(int x = 0; x < ui->lettres->children().count(); x++)
  {
    QPushButton *q = qobject_cast<QPushButton *>(ui->lettres->children().at(x));
    q->setEnabled(true);
  }
}

//*******  METHODE traitement  *******//
void MainWindow::traitement(char lettre)   // le caractère en paramètre correspond au caractère sur lequel on a cliqué
{
  /*  1- on teste ici si le caractère passé en paramètre (lettre) est contenu dans le mot à deviner
   *   Si c'est le cas :
   *      - il faut transférer le mot du tableau à faire deviner dans une variable de type chaine de caractères
   *      - parcourir le mot caractère par caractère et tester si le caractère du mot lu et équivalent à la lettre
   *         passée en paramètre
   *      - si c'est le cas :
   *            > il faut modifier - au bon endroit - le mot en construction
   *            > incrémenter le nombre de bonnes lettres trouvées
   *   sinon :
   *      - incrémenter le nombre d'erreur
   *      - affecter la variable contenant le chemin d'accès pour y placer la bonne image (la suivante)
   *      - afficher l'image dans le label
   *
   *  2- testS pour savoir où en sont les variables erreur et bon
   *    ==> si 'erreur' est à 12, c'est perdu !!!  ==> appel à la méthode 'gestion_fin'
   *    ==> si 'bon' a la même valeur que le nombre de caractères du mot qui est à faire deviner, c'est gagné ! ==> appel à la méthode 'gestion_fin'
   *
   *  3- mettre à jour le label mot_a_deviner
   */

  /****  définition de variables locales (reconnues uniquement dans cette méthode)    ****/
  QString fichier = ":/images/";    // chaine qui contiendra le chemin d'accès au fichier image à afficher
  QString mot_temp;                  // chaine utilisée pour transférer le mot du tableau à faire deviner
  mot_temp = mot[i];
  //
  if(mot[i].contains(lettre))
  {
      for(int c = 0;c < mot[i].length();c++)
      {
          if (mot_temp[c] == lettre)
          {
              chaine[c] = lettre;
              bon++;
          }
      }

  ui->mot_a_deviner->setText(chaine);
  }
  else
  {
      erreur++;
      fichier.append(QString::number(erreur) + ".jpg");
      ui->image->setPixmap(fichier);
  }
  //

  if (erreur ==12)
  {
      gestion_fin('p');
  }
  if (bon == mot[i].length())
  {
      gestion_fin('G');
  }
}


//*******  METHODE gestion_fin  *******//
void MainWindow::gestion_fin(char cas)
{
  /*
   * gère la fin d'une partie : perdu ou gagné
   * 1- le caractère 'cas' passé en paramètre est à tester ici :
   * => si il vaut 'P' , le label message doit être affecté ainsi : texte de couleur rouge (texte doit signaler que c'est perdu !)
   * => sinon (c'est donc gagné !), le label message doit être affecté ainsi : texte de couleur verte -texte doit signaler que c'est gagné !)
   *
   * 2- désactiver le groupbox 'lettres'
   * 3- rendre visibles les boutons 'quitter' et 'rejouer'
   *
   */
  //
  if (cas == 'P')
  {
      ui->message->setStyleSheet("color::red");
      ui->message->setText("Perdu il fallais trouver : " + mot[i]);
  }
  else if (cas == 'G')
  {
      if (i>=10)
      {
          ui->message->setStyleSheet("color::green");
          ui->message->setText("c'est gagner");
      }
      else
      {
          ui->message->setStyleSheet("color::green");
          ui->message->setText("c'est gagner on passe au mot suivant");
      }
  }
  ui->lettres->setEnabled(false);
  ui->rejouer->setVisible(true);
  ui->quitter->setVisible(true);

  //
}

//****** gestion du clic sur bouton rejouer    ******//
void MainWindow::on_rejouer_clicked()
{
  // on incrémente l'indice i pour passer au mot suivant dans le tableau 'mot' // TODO
  //
  // on initialise la fenêtre de jeu   // TODO
  //
    i++;
    initialisation();


}

//******* gestion du clic sur le bouton quitter  ******//  NE PAS MODIFIER
void MainWindow::on_quitter_clicked()
{
  this->close();  // fermeture de la fenêtre
}


/*************************************************************************************
 *
 ***   méthodes gérant le clic sur un bouton lettre ==> ici 1 bouton = 1 méthode   ***
 ***   chaque méthode fait appel à la méthode 'traitement'  et désactive le bouton
 ***   lettre cliqué.
 ***   La méthode 'traitement' est appélée avec en paramètre le caractère correspondant
 ***   au bouton sur lequel on a cliqué.
 *
**************************************************************************************/
void MainWindow::on_A_clicked()
{
  traitement('A');
  ui->A->setEnabled(false);
}

void MainWindow::on_B_clicked()
{
  traitement('B');
  ui->B->setEnabled(false);
}

void MainWindow::on_C_clicked()
{
  traitement('C');
  ui->C->setEnabled(false);
}

void MainWindow::on_D_clicked()
{
  traitement('D');
  ui->D->setEnabled(false);
}

void MainWindow::on_E_clicked()
{
  traitement('E');
  ui->E->setEnabled(false);
}

void MainWindow::on_F_clicked()
{
  traitement('F');
  ui->F->setEnabled(false);
}

void MainWindow::on_G_clicked()
{
  traitement('G');
  ui->G->setEnabled(false);
}

void MainWindow::on_H_clicked()
{
  traitement('H');
  ui->H->setEnabled(false);
}

void MainWindow::on_I_clicked()
{
  traitement('I');
  ui->I->setEnabled(false);
}

void MainWindow::on_J_clicked()
{
  traitement('J');
  ui->J->setEnabled(false);
}

void MainWindow::on_K_clicked()
{
  traitement('K');
  ui->K->setEnabled(false);
}

void MainWindow::on_L_clicked()
{
  traitement('L');
  ui->L->setEnabled(false);
}

void MainWindow::on_M_clicked()
{
  traitement('M');
  ui->M->setEnabled(false);
}

void MainWindow::on_N_clicked()
{
  traitement('N');
  ui->N->setEnabled(false);
}

void MainWindow::on_O_clicked()
{
  traitement('O');
  ui->O->setEnabled(false);
}

void MainWindow::on_P_clicked()
{
  traitement('P');
  ui->P->setEnabled(false);
}

void MainWindow::on_Q_clicked()
{
  traitement('Q');
  ui->Q->setEnabled(false);
}

void MainWindow::on_R_clicked()
{
  traitement('R');
  ui->R->setEnabled(false);
}

void MainWindow::on_S_clicked()
{
  traitement('S');
  ui->S->setEnabled(false);
}

void MainWindow::on_T_clicked()
{
  traitement('T');
  ui->T->setEnabled(false);
}

void MainWindow::on_U_clicked()
{
  traitement('U');
  ui->U->setEnabled(false);
}

void MainWindow::on_V_clicked()
{
  traitement('V');
  ui->V->setEnabled(false);
}

void MainWindow::on_W_clicked()
{
  traitement('W');
  ui->W->setEnabled(false);
}

void MainWindow::on_X_clicked()
{
  traitement('X');
  ui->X->setEnabled(false);
}

void MainWindow::on_Y_clicked()
{
  traitement('Y');
  ui->Y->setEnabled(false);
}

void MainWindow::on_Z_clicked()
{
  traitement('Z');
  ui->Z->setEnabled(false);
}