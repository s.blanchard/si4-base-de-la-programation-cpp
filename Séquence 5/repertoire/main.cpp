#include <iostream>
#include "prototype.h"

using namespace std;

int main()
{
  int choix ;
  string nom;
  InitRepertoire();

  do
  {
    choix = Menu();

    switch(choix)
    {
      case 1 :
        AfficheRep() ;
        cout << endl;
        break ;

      case 2 :
        cout << " quel nom recherchez - vous ? " ;
        cin >> nom;
        ChercherNum(nom);
        break ;

      case 3 :
        AjouterCoordonnees();
        break ;

      case 4 :
        cout << " \n\tA U   R E V O I R  :)" << endl;
        break;

      default :
        cout << "\E[31;1m" << " Erreur de saisie " << "\E[m " << endl;
    }
  }
  while(choix != 4) ;

  return 0;
}
